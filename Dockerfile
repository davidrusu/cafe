FROM ubuntu:latest
RUN apt-get update -y && apt-get install -y python3 python3-pip libpq-dev
RUN mkdir /app
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r /app/requirements.txt
COPY . .

CMD gunicorn -w 4 cafe:app -b :5000
