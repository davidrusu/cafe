from sqlalchemy.orm import validates
from app.model import db

class Drink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1024))
    price = db.Column(db.Float)
    availability_start = db.Column(db.DateTime())
    availability_end = db.Column(db.DateTime())

    def __init__(self, name, price, availability_start, availability_end):
        self.name = name
        self.price = price
        self.availability_start = availability_start
        self.availability_end = availability_end

    
    @validates('price')
    def validate_price(self, key, price):
        assert price >= 0
        return price

    @validates('availability_end')
    def validate_availabilty(self, key, availability_end):
        assert self.availability_start < availability_end
        return availability_end
