from werkzeug.exceptions import BadRequest
from flask import Blueprint, request, jsonify

from app.model import db
from app.model.drink import Drink
from app.util import parse_datetime, format_datetime


drink = Blueprint('drink', __name__)


def jsonify_drink(drink):
    """Drink JSON template"""
    return {
        'id': drink.id,
        'name': drink.name,
        'price': drink.price,
        'availability_start': format_datetime(drink.availability_start),
        'availability_end': format_datetime(drink.availability_end)
    }


@drink.route('/', methods=['POST'])
def new_drink():
    """Create a new drink
    Data should be in the body in JSON form with the structure:
    {
      "name": "java",
      "price": 1.234,
      "availability_start": "09-12-2016 00:00",
      "availability_end": "09-20-2016 00:00"
    }
    """
    
    data = request.get_json()

    for key in ['name', 'price', 'availability_start', 'availability_end']:
        if key not in data:
            raise BadRequest(description="key: '{}' missing".format(key))
    
    
    drink = Drink(name=data['name'],
                  price=data['price'],
                  availability_start=parse_datetime(data['availability_start']),
                  availability_end=parse_datetime(data['availability_end']))

    db.session.add(drink)
    db.session.commit()

    return jsonify(jsonify_drink(drink)), 201


@drink.route('/<drink_id>', methods=['DELETE'])
def delete_drink(drink_id):
    """Delete the drink with the given drink id"""
    
    drink = Drink.query.get(drink_id)
    if drink is None:
        raise BadRequest(description="Drink doesn't exist")

    db.session.delete(drink)
    db.session.commit()
    return "Success", 200


@drink.route('/query', methods=['GET'])
def query_drinks():
    """ Search for drinks by name and date
    Args should be in the query string:
    name: (Optional) String            --- drinks with this name
    date: (Optional) MM-DD-YYY HH:MM   --- drinks that are available at this date
    """
    
    name = request.args.get('name', None)
    date = request.args.get('date', None)

    query = Drink.query
    if name is not None:
        query = query.filter_by(name=name)

    if date is not None:
        date = parse_datetime(date)
        query = query.filter(Drink.availability_start <= date,
                     Drink.availability_end > date)
    
    results = query.all()
    jsonified = [ jsonify_drink(d) for d in results ]
    return jsonify(jsonified), 200
