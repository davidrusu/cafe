from flask import Flask

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_pyfile(config_filename)

    from app.model import db
    db.init_app(app)
    
    from app.api.drink import drink as drink_api
    app.register_blueprint(drink_api, url_prefix='/api/drink')
    return app, db
