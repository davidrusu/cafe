from datetime import datetime

DATETIME_FORMAT = "%m-%d-%Y %H:%M"

def parse_datetime(datetime_str):
    return datetime.strptime(datetime_str, DATETIME_FORMAT)

def format_datetime(dt):
    return dt.strftime(DATETIME_FORMAT)
    
