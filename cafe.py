from app import create_app
from flask_migrate import Migrate


app, db = create_app('config.cfg')
migrate = Migrate(app, db)


@app.route('/routes')
def routes():
    def format_rule(r):
        return "{}\t({})\t-> {}".format(r.rule,
                                      " | ".join(r.methods),
                                      r.endpoint)
    rules = [format_rule(r) for r in app.url_map.iter_rules()]
    return "<pre>" + "\n".join(rules) + "</pre>"


if __name__ == "__main__":
    app.run(debug=True)
