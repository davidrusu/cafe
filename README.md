# Cafe

## To get it running
In the Cafe directory run:

``` bash
docker-compose build
docker-compose run cafe flask db upgrade    # runs the database migrations
docker-compose up -d
```

Now it should be accepting requests on port 80.

## For local (non-docker) dev
```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt

# somehow setup postgres so that it's accessible on 0.0.0.0:5432

flask db upgrade    # run migration scripts
flask run           # start the server
```

If you modify models, need to create a migration
```
flask db migrate    # this will run alembic and will detect changes to models
flask db upgrade    # run the newly created migrations
```

## API Docs

### Drink API

#### `GET /api/drink/query`
Query string args:
- name: (Optional) String 
- date: (Optional) MM-DD-YYY HH:MM

##### Response
Request: `/api/drink/query?name=java&date=09-13-2016+00:00`

``` javascript
[
  {
    "availability_end": "09-20-2016 00:00",
    "availability_start": "09-12-2016 00:00",
    "id": 2,
    "name": "java",
    "price": 1.234
  }
]
```

#### `POST /api/drink/`
JSON body:
``` javascript
{
	"name": "java",
	"price": 1.234,
	"availability_start": "09-12-2016 00:00",
	"availability_end": "09-20-2016 00:00"
}
```

##### Response
``` javascript
{
  "availability_end": "09-20-2016 00:00",
  "availability_start": "09-12-2016 00:00",
  "id": 3,
  "name": "java",
  "price": 1.234
}
```

#### `DELETE /api/drink/<drink_id>`
In path:
- drink_id: the id of the drink to remove

e.g. `DELETE /api/drink/231`

##### Response
```
Success
```
